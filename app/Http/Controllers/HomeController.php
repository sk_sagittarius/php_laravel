<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index(Request $request)
    {
//        $books = Book::first();
//        echo $books->name . PHP_EOL;
//        echo $books->year;
        //dd($books); //die and dump

//        $book = new Book();
//        $book->name='Lord of the Rings';
//        $book->year=1995;
//        $book->save();

//        $id = $request->get('id', 1);
//        $book = Book::find($id);
//        if($book===null)
//            return response('',404);

        return 'index';
    }

    function book(Book $book)
    {
        return $book;
    }
}
